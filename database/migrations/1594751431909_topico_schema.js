'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TopicoSchema extends Schema {
  up () {
    this.create('topicos', (table) => {
      table.increments()
      table.string('nome')
      table.string('descricao')
      table.integer('categoria_id').references('id').inTable('categorias')
      table.timestamps()
    })
  }

  down () {
    this.drop('topicos')
  }
}

module.exports = TopicoSchema
