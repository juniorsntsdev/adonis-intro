'use strict'

const Resposta = use('App/Models/Resposta')

class RespostaController {
  
    async index({ request, response }){
      let respostas = await Resposta.all();
      return response.status(200).send(respostas);
    }

    async store({ request, response }){
      const { resposta, topico_id } = await request.post();
      let novaResposta = await Resposta.create({
        resposta,
        topico_id
      });
      return response.status(201).send(novaResposta);
    }
}

module.exports = RespostaController
