'use strict'

const Route = use('Route')

Route.get('/test', () => {
  return { greeting: 'Hello world in AdonisJS' }
});

Route.group(() => {
  Route.resource('/categorias', 'CategoriaController').only(['index', 'store']);
  Route.resource('/topicos', 'TopicoController').only(['index', 'store']);
  Route.resource('/respostas', 'RespostaController').only(['index', 'store']);
}).prefix('api').middleware(['auth:jwt']);
